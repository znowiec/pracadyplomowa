﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System.Windows.Input;
using RandomNumGenerator.Model;

namespace RandomNumGenerator.ViewModel
{
    public class MainViewModel
    {
        public ObservableCollection<Reporter> Reporter { get; set; }

        public ICommand StartGeneratorCommand { get; set; }

        public MainViewModel()
        {
            Initialize();
            StartGeneratorCommand = new RelayCommand(() =>
            {
                Reporter[0].StartGeneratorAsync();
            });
             
        }

        private void Initialize()
        {
            ObservableCollection<Reporter> r = new ObservableCollection<Reporter>();
            r.Add(new Reporter());

            Reporter = r;
        }

        private void StartGenerator()
        {
            Reporter[0].StartGeneratorAsync();
        }
         
        private void ClearModel()
        {
            Reporter.Clear();
            Reporter.Add(new Reporter());
        }
    }
}
