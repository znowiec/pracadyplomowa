﻿using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace RandomNumGenerator.Utils
{
    public class SimpleTextToastManager
    {
        public static SimpleTextToastManager Instance { get; }
            = new SimpleTextToastManager();
        private SimpleTextToastManager() { }

        public void ShowToast(string line1, string line2 = null)
        {
            var template = $@"
<toast>
<visual>
<binding template=""ToastGeneric"">
  <text>{line1}</text>
  <text>{line2}</text>
</binding>
</visual>
</toast>
";
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(template);
            ToastNotification toast = new ToastNotification(xml);
            //toast.SuppressPopup = true;
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
