﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Popups;

namespace RandomNumGenerator.Model
{
    public class CameraGenerator : AbstractGenerator
    {
        public CameraGenerator(int maxCountRandomNumbers) : base(maxCountRandomNumbers)
        {
        }

        public async Task RunGeneratorAsync()
        {
            StorageFile resultFile = await GetResultFile();
            if(resultFile == null)
            {
                await new MessageDialog("Nie wybraono pliku zapisu liczb. Operacja zostanie przerwana.").ShowAsync();
                return;
            }

            //test
            /*
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary; 
            picker.FileTypeFilter.Add(".png");
            StorageFile photoFile = await picker.PickSingleFileAsync(); */

            StorageFile photoFile = await TakePhotoAsync();
            if (photoFile == null)
            {
                await new MessageDialog("Nie zostało zrobione zdjęcie. Operacja zostanie przerwana.").ShowAsync();
                return;
            }

            if (photoFile == null)
            {
                return;
            }
            if (resultFile == null)
            {
                return;
            }

            using (IRandomAccessStream stream = await photoFile.OpenAsync(FileAccessMode.Read))
            {
                BitmapDecoder decoder = await BitmapDecoder.CreateAsync(stream);

                SoftwareBitmap softwareBitmap = await decoder.GetSoftwareBitmapAsync();
                StartTime = DateTime.Now;
                await GenerateNumbers(softwareBitmap, BitmapEncoder.PngEncoderId, resultFile); 
            }
        }

        private async Task<byte[]> GetByteArrayFromPhotoAsync(SoftwareBitmap softwareBitmap, Guid encoderId)
        {
            using (var ms = new InMemoryRandomAccessStream())
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(encoderId, ms);
                encoder.SetSoftwareBitmap(softwareBitmap);

                byte[] array = null;
                try
                {
                    await encoder.FlushAsync();
                }
                catch (Exception ex) { return new byte[0]; }

                array = new byte[ms.Size];
                await ms.ReadAsync(array.AsBuffer(), (uint)ms.Size, InputStreamOptions.None);
                return array;
            }
        }

        private async Task GenerateNumbers(SoftwareBitmap softwareBitmap, Guid encoderId, StorageFile resultFile)
        {
            StartTime = DateTime.Now;
            byte[] array = await GetByteArrayFromPhotoAsync(softwareBitmap, encoderId);

            UseNumberArray = null; 
            string text = string.Empty;  //;await Windows.Storage.FileIO.ReadTextAsync(outputFile);
            using (IRandomAccessStream textStream = await resultFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                using (DataWriter textWriter = new DataWriter(textStream))
                {
                    textWriter.WriteString(FileHeader); 
                    int number;
                    string s = string.Empty;
                    string sb = string.Empty;
                    string other = string.Empty;
                    int maxLength = 9;
                    List<byte> listArray = array.ToList();
                    array = null;
                    var rnd = new Random(DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond);
                    string item = string.Empty;
                    while (listArray.Count != 0 && UseNumberArray.Count < MaxCountRandomNumbers)
                    {
                        int index = rnd.Next(0, listArray.Count); 
                        byte b = listArray[index];
                        listArray.RemoveAt(index); // remove current random number from list
                         
                        s += string.Format("{0}", Convert.ToInt32(b)); 
                        if (s.Length > maxLength)
                        {
                            if (!Int32.TryParse((s.Length > 10 ? s.Remove(10, s.Length - 10) : s), out number))
                            {
                                if (!Int32.TryParse(s.Remove(maxLength, s.Length - maxLength), out number))
                                {
                                    continue;
                                }
                            }
                            if (!UseNumberArray.Contains(number))
                            {
                                UseNumberArray.Add(number);
                                item = number.ToString();
                                if (item.Length == 10)
                                    item = item[rnd.Next(0, 9)] + item.Remove(0, 1);
                                while (item.Length < 10)
                                {
                                    item = " " + item;
                                }
                                textWriter.WriteString(Environment.NewLine + item);
                            }
                            s = s.Length > 10 ? s.Remove(0, 10) : string.Empty;
                        }
                    }
                    EndTime = DateTime.Now;
                    await textWriter.StoreAsync();
                    await new MessageDialog("Save file.").ShowAsync();
                    OnGeneratorStoped();
                }
            }
            UseNumberArray = null;
        }

        private async Task<StorageFile> TakePhotoAsync()
        {
            CameraCaptureUI captureUI = new CameraCaptureUI();
            captureUI.PhotoSettings.Format = CameraCaptureUIPhotoFormat.Png;
            captureUI.PhotoSettings.AllowCropping = false;
            StorageFile inputFile = await captureUI.CaptureFileAsync(CameraCaptureUIMode.Photo);
            return inputFile;
        }

    }
}
