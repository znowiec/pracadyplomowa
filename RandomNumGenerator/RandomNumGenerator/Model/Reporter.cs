﻿using RandomNumGenerator.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Sensors;
using Windows.UI.Popups;

namespace RandomNumGenerator.Model
{
    public class Reporter : INotifyPropertyChanged, IDisposable
    {
        private AbstractGenerator generator;
        private SimpleTextToastManager simpleTextToastManager = SimpleTextToastManager.Instance;
        public enum GeneratorTypes
        {
            Accelerometer,
            Camera
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public GeneratorTypes CurrentGeneratorType
        {
            get
            {
                return (_cameraTypeChecked ? GeneratorTypes.Camera : GeneratorTypes.Accelerometer);
            }
        }

        private bool _accelerometerTypeChecked = true;
        public bool AccelerometerTypeChecked
        {
            get
            {
                return _accelerometerTypeChecked;
            }
            set
            {
                _accelerometerTypeChecked = value;
                _cameraTypeChecked = !value;

                RaisePropertyChanged("CameraTypeChecked ");
                RaisePropertyChanged("AccelerometerTypeChecked");
            }
        }

        private bool _cameraTypeChecked = false;
        public bool CameraTypeChecked
        {
            get
            {
                return _cameraTypeChecked;
            }
            set
            {
                _cameraTypeChecked = value;
                _accelerometerTypeChecked = !value;

                RaisePropertyChanged("CameraTypeChecked ");
                RaisePropertyChanged("AccelerometerTypeChecked");
            }
        }


        private bool _isRunning = false;
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
            set
            {
                _isRunning = value;
                RaisePropertyChanged("IsRunning");
                RaisePropertyChanged("IsNotRunning");
            }
        }

        public bool IsNotRunning
        {
            get
            {
                return !_isRunning;
            }
        }
        /*List<TestSample<AccelerometerPoints>> _samples;
        List<TestSample<AccelerometerPoints>> Samples
        {
            get
            {
                if (_samples == null)
                    _samples = new List<TestSample<AccelerometerPoints>>();
                return _samples;
            }
            set
            {
                _samples = value;
                RaisePropertyChanged("Samples");
            }
        }
        */
        private double _totalSecondsWorkTime = 0;
        public double TotalSecondsWorkTime
        {
            get
            {
                return _totalSecondsWorkTime;
            }
            set
            {
                _totalSecondsWorkTime = value;
                RaisePropertyChanged("TotalSecondsWorkTime");
            }

        }

        int _samplesCount = 0;
        public int SamplesCount
        {
            get
            {
                return _samplesCount;
            }
            set
            {
                this._samplesCount = value;
                RaisePropertyChanged("SamplesCount");
            }
        }
        DateTime? _startGeneratorTime;
        public DateTime? StartGeneratorTime
        {
            get
            {
                return this._startGeneratorTime;
            }
            set
            {
                this._startGeneratorTime = value;
                RaisePropertyChanged("StartGeneratorTime");
            }
        }

        DateTime? _endGeneratorTime;
        public DateTime? EndGeneratorTime
        {
            get
            {
                return this._endGeneratorTime;
            }
            set
            {
                this._endGeneratorTime = value;
                RaisePropertyChanged("EndGeneratorTime");
            }
        }
        public Reporter()
        {
        }
         
        public async Task StartGeneratorAsync()
        {
            generator = null;
            int randomNumCount = 200; 

            //IsRunning = true;
            if (CurrentGeneratorType == GeneratorTypes.Camera)
            {
                generator = new CameraGenerator(randomNumCount);
                await (generator as CameraGenerator).RunGeneratorAsync();
            }
            if (CurrentGeneratorType == GeneratorTypes.Accelerometer)
            {
                generator = new AccelerometerGenerator(randomNumCount);
                await (generator as AccelerometerGenerator).RunGeneratorAsync();
            }
            if(generator != null)
            {
                generator.OnSampleAdd += Generator_OnSampleAdd;
                generator.OnGeneratorStop += Generator_OnGeneratorStop;
            }
        }

        private void Generator_OnGeneratorStop(object sender, GenerateEndedEventArgs e)
        {
            Task.WaitAll(
                Task.Run(async () => 
                    await new MessageDialog(
                            string.Format("Czas generowania {0} liczb to {1} sekund.", e.SamplesCount, e.TotalSecondsWorkTime)
                            ).ShowAsync())
                        );  

            //IsRunning = false;
        } 

        private void Generator_OnSampleAdd(object sender, GenerateNewSampleEventArgs e)
        {
      //      if (e.SamplesCount%1000 == 0)
            //    simpleTextToastManager.ShowToast(e.SamplesCount.ToString());
        }
         
        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
