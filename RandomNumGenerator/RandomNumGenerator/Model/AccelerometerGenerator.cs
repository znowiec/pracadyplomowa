﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Sensors;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;

namespace RandomNumGenerator.Model
{
    public class AccelerometerGenerator : AbstractGenerator
    {
        public AccelerometerGenerator(int maxCountRandomNumbers) : base(maxCountRandomNumbers)
        {
        }

        Accelerometer _accelerometer;
        StorageFile resultFile;
        public async Task RunGeneratorAsync()
        {
            resultFile = await GetResultFile();
            if (resultFile == null)
            {
                await new MessageDialog("Nie wybraono pliku zapisu liczb. Operacja zostanie przerwana.").ShowAsync();
                return;
            }

            if (resultFile == null)
            {
                return;
            }

            if (_accelerometer == null)
            {
                _accelerometer = Accelerometer.GetDefault();
                _accelerometer.ReportInterval = 1;
                StartTime = DateTime.Now;
                _accelerometer.ReadingChanged += _accelerometer_ReadingChanged; 
            }
        }
        string sep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        private Int32 ConvertReading(double arg)
        {
            Int32 number;
            string s = arg.ToString().Replace(sep, "").Replace("-", "");
            {
                if (!Int32.TryParse(s, out number))
                {
                    if (!Int32.TryParse((s.Length > 10 ? s.Remove(10, s.Length - 10) : s), out number))
                    {
                        if (!Int32.TryParse(s.Remove(10, s.Length - 10), out number))
                        {
                            return Int32.MaxValue;
                        }
                    }
                }
            }
            return number;
        }

        private void CheckAndAddNumber(double number)
        {
            int num = ConvertReading(number);
            if (!UseNumberArray.Contains(num))
            {
                UseNumberArray.Add(num);
                OnSampleAdded(num);
            }
            if (UseNumberArray.Count == MaxCountRandomNumbers)
            {
                EndTime = DateTime.Now;
                _accelerometer.ReadingChanged -= _accelerometer_ReadingChanged;
                try
                {
                    Task.WaitAll(Task.Run(async () => await SerializeSamplesToFileAsync())); //trick na wywolanie asynchroniczne w evencie
                }
                catch
                {
                    OnGeneratorStoped();
                    throw new Exception("Wystąpił błąd podczas serializacji wyniku."); 
                }
            }
        }

        private void _accelerometer_ReadingChanged(Accelerometer sender, AccelerometerReadingChangedEventArgs args)
        {
            CheckAndAddNumber(args.Reading.AccelerationX);  //składowa X
            CheckAndAddNumber(args.Reading.AccelerationY);  //składowa Y
            CheckAndAddNumber(args.Reading.AccelerationZ);  //składowa Z
        }

        private async Task SerializeSamplesToFileAsync()
        {
            string item = string.Empty;
            using (IRandomAccessStream textStream = await resultFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                using (DataWriter textWriter = new DataWriter(textStream))
                {
                    textWriter.WriteString(FileHeader);

                    foreach (int number in UseNumberArray)
                    {
                        item = number.ToString();
                        while (item.Length < 10)
                        {
                            item = " " + item;
                        }
                        textWriter.WriteString(Environment.NewLine + item);
                    }
                    OnGeneratorStoped();
                    await textWriter.StoreAsync();
                    await new MessageDialog("Save file.").ShowAsync();
                }
            }
        }

    }
}
