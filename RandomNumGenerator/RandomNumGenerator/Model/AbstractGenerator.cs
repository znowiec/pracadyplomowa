﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;

namespace RandomNumGenerator.Model
{
    public class AbstractGenerator
    {
        public event EventHandler<GenerateNewSampleEventArgs> OnSampleAdd;
        public event EventHandler<GenerateEndedEventArgs> OnGeneratorStop;

        private DateTime _startTime;
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime;
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private int _maxCountRandomNumbers;
        public int MaxCountRandomNumbers
        {
            get { return _maxCountRandomNumbers; }
            set { _maxCountRandomNumbers = value; }
        }

        private List<int> _useNumberArray;
        public List<int> UseNumberArray
        {
            get
            {
                if (_useNumberArray == null) _useNumberArray = new List<int>();
                return _useNumberArray;
            }
            set { _useNumberArray = value; }
        }

        public int SamplesCount
        {
            get
            {
                if (_useNumberArray == null) return 0;
                return _useNumberArray.Count;
            }
        }

        protected string FileHeader
        {
            get
            {
                string sHeader = @"#==============================================
# generator based on {1}  
#=============================================
type: d
count: {0}
numbit: 32";
                sHeader = string.Format(sHeader, MaxCountRandomNumbers, (this is CameraGenerator ) ? "Camera" : "Accelerometer");
                return sHeader;
            }
        }

        public AbstractGenerator(int maxCountRandomNumbers)
        {
            MaxCountRandomNumbers = maxCountRandomNumbers;
        }

        protected async Task<StorageFile> GetResultFile()
        {
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            savePicker.FileTypeChoices.Add("Plain Text", new List<string>() { ".txt" });
            savePicker.SuggestedFileName = ((this is CameraGenerator) ? "Camera" : "Accelerometer") + "_Samples_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");

            StorageFile file = await savePicker.PickSaveFileAsync();
            return file;
        }

        protected void OnSampleAdded(int? currentGenerateNumber)
        {
            if(OnSampleAdd != null)
            {
                OnSampleAdd(this, new GenerateNewSampleEventArgs(UseNumberArray.Count, currentGenerateNumber));
            }
        }

        protected void OnGeneratorStoped()
        {
            if(OnGeneratorStop != null)
            {
                OnGeneratorStop(this, new GenerateEndedEventArgs(UseNumberArray.Count, (EndTime - StartTime).TotalSeconds));
            }
        }
    }

    public class GenerateNewSampleEventArgs : EventArgs
    {
        private int _samplesCount = 0;
        public int SamplesCount
        {
            get { return _samplesCount; }
            private set { _samplesCount = value; }
        }

        private int? _currentGenerateNumber;
        public int? CurrentGenerateNumber
        {
            get { return _currentGenerateNumber; }
            private set { _currentGenerateNumber = value; }
        }

        public GenerateNewSampleEventArgs(int samplesCount, int? currentGenerateNumber)
        {
            SamplesCount = samplesCount;  //ilość wygenerowanych liczb
            CurrentGenerateNumber = currentGenerateNumber;  //ilosc
        }
    }

    public class GenerateEndedEventArgs : EventArgs
    {
        private double _totalSecondsWorkTime = 0;
        public double TotalSecondsWorkTime
        {
            get { return _totalSecondsWorkTime; }
            private set { _totalSecondsWorkTime = value; }
        }

        private int _samplesCount = 0;
        public int SamplesCount
        {
            get { return _samplesCount; }
            private set { _samplesCount = value; }
        }

        public GenerateEndedEventArgs(int samplesCount, double totalSecondsWorkTime)
        {
            SamplesCount = samplesCount;    //ilość wygenerowanych liczb
            TotalSecondsWorkTime = totalSecondsWorkTime;    //czas generacji w sekundach
        }
    }
}

